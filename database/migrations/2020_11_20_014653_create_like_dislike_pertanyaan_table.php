<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->unsignedBigInteger('pertanyaan_Id')->nullable();
            $table->unsignedBigInteger('profile_Id')->nullable();
            
            $table->foreign('pertanyaaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->integer('poin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan');
    }
}
